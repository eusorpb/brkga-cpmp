# README #

This project is an metaheuristic solver for the container pre-marshalling problem using a biased random-key genetic algorithm. This work has been submitted to Computers and Operations Research. Detailed documentation for installing and using the software will be coming soon. For now, we recommend cloning the repository, building the code with xbuild and viewing the help file.

### Installation / Setup ###

* TODO

### Citing this work ###

* TODO